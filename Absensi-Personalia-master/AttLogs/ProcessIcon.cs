﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace AttLogs
{
    class ProcessIcon
    {
        NotifyIcon ni;
        string timenow = "";
        bool isAbout = false;
        Attendance a = new Attendance();

        List<string> arrWithdrawalTime;

        public ProcessIcon()
        {
            // Instantiate the NotifyIcon object.
            ni = new NotifyIcon();

            arrWithdrawalTime = CommonLibrary.Instance.getWithdrawalTime();

            Timer t1 = new Timer();
            t1.Interval = 1000;
            t1.Tick += new EventHandler(t1_Tick);
            t1.Start();
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss"));
        }

        public void Display()
        {
            // Put the icon in the system tray and allow it react to mouse clicks.			
            //ni.MouseClick += new MouseEventHandler(ni_MouseClick);
            ni.Icon = Resources.icon2;
            ni.Text = "Personalia Attendance System";
            ni.Visible = true;

            ni.BalloonTipTitle = "Personalia Attendance System";
            ni.BalloonTipText = "is running";
            ni.ShowBalloonTip(100);

            // Attach a context menu.
            ni.ContextMenuStrip = new ContextMenus().Create();
        }
        public void Dispose()
        {
            // When the application closes, this will remove the icon from the system tray immediately.
            ni.Dispose();
        }


        private void t1_Tick(object sender, EventArgs e)
        {
            timenow = DateTime.Now.ToString("HH:mm:ss");
            Console.WriteLine(string.Format("void t1_Tick {0}", timenow));
            bool result = arrWithdrawalTime.Contains(timenow);
            //
            if (result == true)
            {
                //Console.WriteLine("Start");
                a.pullAttendance();
            }

        }
    }
}

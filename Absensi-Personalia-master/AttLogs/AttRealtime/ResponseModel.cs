﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttRealtime
{
    public class ResponseModel
    {
        public bool IsSucces { get; set; }
        public string ReturnMessage { get; set; }
    }
}

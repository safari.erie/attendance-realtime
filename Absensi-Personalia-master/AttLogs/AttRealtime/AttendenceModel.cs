﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttRealtime
{
    class AttendenceModel
    {
        public string MachineId { get; set; }
        public string IpAddress { get; set; }
        public string RecordFlag { get; set; }
        public string PersonId { get; set; }
        public string AttendanceDt { get; set; }
        public string Status { get; set; }

        public void SetAttendance(string _MachineId, string _IpAddres, string _RecordFlag, string _PersonId, string _AttendanceDt, string _Status)
        {
            MachineId = _MachineId;
            IpAddress = _IpAddres;
            RecordFlag = _RecordFlag;
            PersonId = _PersonId;
            AttendanceDt = _AttendanceDt;
            Status = _Status;
        }
    }
}

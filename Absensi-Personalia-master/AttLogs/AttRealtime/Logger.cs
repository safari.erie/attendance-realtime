﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttRealtime
{
    class Logger
    {
        private readonly ILog oLogger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Logger(string _LogFileName, string _Appname)
        {
            this.Setup(_LogFileName, _Appname);
        }

        public void Setup(string LogFileName, string appName)
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = string.Format("%date [%thread] %-5level {0} - %message%newline", appName);
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            roller.AppendToFile = true;
            roller.File = string.Format(@"Logs\{0}.txt", LogFileName);
            roller.Layout = patternLayout;
            roller.MaxSizeRollBackups = 5;
            roller.MaximumFileSize = "100MB";
            roller.RollingStyle = RollingFileAppender.RollingMode.Size;
            roller.StaticLogFileName = true;
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);

            MemoryAppender memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);

            hierarchy.Root.Level = Level.Info;
            hierarchy.Configured = true;
        }

        public void WriteLog(string LogMessages, string LogType = "INF")
        {
            if (LogType == "INF")
            {
                oLogger.Info(LogMessages);
            }
            else if (LogType == "ERR")
            {
                oLogger.Error(LogMessages);
            }
            else if (LogType == "WRN")
            {
                oLogger.Warn(LogMessages);
            }
        }
    }
}

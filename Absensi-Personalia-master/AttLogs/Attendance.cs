﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace AttLogs
{
    class Attendance : Form
    {
        string server = AttLogs.Properties.Settings.Default.Server;
        int Password = AttLogs.Properties.Settings.Default.Password;
        int Port = AttLogs.Properties.Settings.Default.Port;
        
        string apppath = Application.StartupPath.ToString();
        private bool bIsConnected = false;
        private int iMachineNumber = 1;

        private zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();

        string btnConnectText = "Connect";
        string path = "";

        string messages = "";


        //ArrayList arr_upload_ip = new ArrayList();

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "Attendance";
            this.Load += new System.EventHandler(this.Attendance_Load);
            this.ResumeLayout(false);
        }

        private void Attendance_Load(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        #region Connect to machine
        private void connectToMachine(string ipMachine)
        {
            axCZKEM1 = new zkemkeeper.CZKEMClass();
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;

            axCZKEM1.SetCommPassword(Password);
            bIsConnected = axCZKEM1.Connect_Net(ipMachine, Port);
            if (bIsConnected == true)
            {
                btnConnectText = "DisConnect";
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                CommonLibrary.Instance.writeLog(path, DateTime.Now.ToString("HH:mm:ss") + " - Unable to connect the device [" + ipMachine + "],ErrorCode=" + idwErrorCode.ToString());

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "Unable to connect the device [" + ipMachine + "],ErrorCode=" + idwErrorCode.ToString();
                CommonLibrary.Instance.writeLogApp(messages);
            }
            Cursor = Cursors.Default;
        }
        #endregion

        public void pullAttendance()
        {
            List<ComboboxItem> listIpMachine = CommonLibrary.Instance.getIpMachine();

            if (listIpMachine.Count > 0) {
                foreach (ComboboxItem data in listIpMachine)
                {
                    if (data.Text != "" && data.Text != "All")
                    {
                        path = apppath + @"\log\AutoPullData_" + data.Text + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                        CommonLibrary.Instance.createLogFile(path, data.Text);
                        this.getAttLogs(data.Text);
                    }
                }

                string _dtFrom = string.Format("{0:yyyy-MM-dd}", DateTime.Now.AddDays(-1));
                string _dtTo = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
                string param = _dtFrom + "~" + _dtTo;

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Sync Data. ";
                CommonLibrary.Instance.writeLogApp(messages);

                CommonLibrary.Instance.processTransAttendance("syncTransAttendance", param);
            }
        }

        public void getAttLogs(string ipMachine)
        {
            connectToMachine(ipMachine);

            string errMsg = "";
            string sdwEnrollNumber = "";
            string attendanceDate = "";
            string attendanceDateX = "";
            string data = "";
            string httpString = "";

            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;
            int idwErrorCode = 0;

            int iGLCount = 0;
            int iIndex = 0;

            string today = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            string todayMin1 = string.Format("{0:yyyy-MM-dd}", DateTime.Now.AddDays(-1));

            int enrollNo = 0;
            int status = 0;
            
            var Cursor = Cursors.WaitCursor;

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Reading data from machine "+ ipMachine;
            CommonLibrary.Instance.writeLogApp(messages);

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                               out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    attendanceDate = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(idwYear.ToString("D2") + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2")));
                    attendanceDateX = idwYear.ToString() + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2") + " " + idwHour.ToString("D2") + ":" + idwMinute.ToString("D2") + ":" + idwSecond.ToString("D2");

                    if (today == attendanceDate || todayMin1 == attendanceDate)
                    {
                        iGLCount++;
                        enrollNo = Convert.ToInt32(sdwEnrollNumber);
                        status = idwInOutMode;
                        iIndex++;

                        httpString += ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDateX, status, ipMachine);
                        //data = ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDateX, status, ipMachine);
                        data += ((iGLCount == 1) ? "" : Environment.NewLine) + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDateX, status, ipMachine);
                    }
                }

                CommonLibrary.Instance.writeLog(path, data);

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Push data machine " + ipMachine;
                CommonLibrary.Instance.writeLogApp(messages);

                bool result = CommonLibrary.Instance.pushData("pushData", httpString);
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    errMsg = "Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString();

                    
                }
                else
                {
                    errMsg = "No data from terminal returns!";
                }
                CommonLibrary.Instance.writeLog(path, errMsg);

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + errMsg;
                CommonLibrary.Instance.writeLogApp(messages);
            }

            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
            axCZKEM1.Disconnect();

            //CommonLibrary.Instance.writeLog(path, "finish process " + ipMachine);

        }
        
    }
}

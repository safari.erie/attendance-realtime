﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AttLogs
{
    public partial class AttLogsMain : Form
    {
        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.

        string server = AttLogs.Properties.Settings.Default.Server;
        int Password = AttLogs.Properties.Settings.Default.Password;
        int Port = AttLogs.Properties.Settings.Default.Port;

        string apppath = Application.StartupPath.ToString();
        string _httpString = "";
        string dataAttendance = "";
        string messages = "";

        List<ComboboxItem> listIpMachine;

        public AttLogsMain()
        {
            InitializeComponent();
        }
        private void AttLogsMain_Load(object sender, EventArgs e)
        {
            listIpMachine = CommonLibrary.Instance.getIpMachine();
            this.fillComboBox();
            txtPort.Text = Port.ToString();
        }

        #region event
        #region on Connect / Disconnect
        private void btnConnect_Click(object sender, EventArgs e)
        {
            //lblStatus.Text = "trying to connect to the machine";
            if ( cmbIpMachine.SelectedItem.ToString() == "" || txtPort.Text.Trim() == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                btnPush.Enabled = false;

                return;
            }
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnConnect.Text = "Connect";
                lblState.Text = "Current State:DisConnected";
                Cursor = Cursors.Default;
                return;
            }
            axCZKEM1.SetCommPassword(Password);
            bIsConnected = axCZKEM1.Connect_Net(cmbIpMachine.SelectedItem.ToString(), Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblState.Text = "Current State:Connected";
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "Unable to connect the device,ErrorCode=" + idwErrorCode.ToString();
                CommonLibrary.Instance.writeLogApp(messages);
            }
            Cursor = Cursors.Default;
            btnPush.Enabled = false;
            btnPull.Enabled = true;
            lblStatus.Text = "";
        }
        #endregion

        #region Pull Data From Machine
        private void btnPull_Click(object sender, EventArgs e)
        {
            if (cmbIpMachine.SelectedItem.ToString() == "All")
            {
                lvLogs.Items.Clear();
                int i = 1;
                foreach (ComboboxItem data in listIpMachine)
                {
                    if (data.Text != "" && data.Text != "All")
                    {
                        this.connectToMachine(data.Text);
                        this.getAttLogs(data.Text, i);
                    }
                    i++;
                }

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Pull Data Complete.";
                CommonLibrary.Instance.writeLogApp(messages);

                MessageBox.Show("Pull Data Complete", "Information");
                btnPush.Enabled = true;
            }
            else {
                this.getAttLogsByIp();
            }
        }
        #endregion

        #region Push Data
        private void btnPush_Click(object sender, EventArgs e)
        {
            //string path = apppath + @"\log\ManualPullData_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";

            string _dtFrom = dtPickerFrom.Value.ToString("yyyy-MM-dd");
            string _dtTo = dtPickerTo.Value.ToString("yyyy-MM-dd");
            string param = _dtFrom + "~" + _dtTo;

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Sync Data. ";
            CommonLibrary.Instance.writeLogApp(messages);

            bool result = CommonLibrary.Instance.pushData("pushData", dataAttendance);
            bool res = CommonLibrary.Instance.processTransAttendance("syncTransAttendance", param);
            //Console.WriteLine("Finish Upload");

            //CommonLibrary.Instance.writeLog(path, dataAttendance);
            MessageBox.Show("Success","Information");

            //btnPush.Enabled = false;
            //btnPull.Enabled = true;
        }
        #endregion

        #region on Selected ip Machine
        private void cmbIpMachine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbIpMachine.SelectedItem.ToString() == "All")
            {
                btnConnect.Enabled = false;
                btnPull.Enabled = true;
            }
            else {
                btnConnect.Enabled = true;
                btnPull.Enabled = false;

                if (btnConnect.Text == "DisConnect")
                {
                    axCZKEM1.Disconnect();
                    bIsConnected = false;
                    btnConnect.Text = "Connect";
                    btnConnect.ForeColor = Color.FromArgb(48, 104, 193);
                    lblState.Text = "Current State:DisConnected";
                    Cursor = Cursors.Default;
                    return;
                }
            }
        }
        #endregion
        
        #endregion

        #region Connect to machine
        private void connectToMachine(string ipMachine)
        {
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;

            axCZKEM1.SetCommPassword(Password);
            bIsConnected = axCZKEM1.Connect_Net(ipMachine, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.ForeColor = Color.FromArgb(193, 48, 48);
                btnConnect.Refresh();
                lblState.Text = "Current State:Connected";
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");

                messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "Unable to connect the device,ErrorCode=" + idwErrorCode.ToString();
                CommonLibrary.Instance.writeLogApp(messages);
            }
            Cursor = Cursors.Default;
        }
        #endregion

        #region Get AttLogs By IP
        private void getAttLogsByIp()
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first", "Error");
                return;
            }

            string sdwEnrollNumber = "";
            string attendanceDate = "";
            string ipMachine = cmbIpMachine.Text;
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;
            int idwErrorCode = 0;
            //int iValue = 0;
            int iGLCount = 0;
            int iIndex = 0;
            int enrollNo = 0;
            int status = 0;
            int j = 1;

            //string msg = "";
            string httpString = "";
            string data = "";
            string path = apppath + @"\log\ManualPullData_" + ipMachine + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            //string path = apppath + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            string today = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            string _dtFrom = dtPickerFrom.Value.ToString("yyyy-MM-dd");
            string _dtTo = dtPickerTo.Value.ToString("yyyy-MM-dd");

            DateTime dtFrom = DateTime.ParseExact(_dtFrom, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(_dtTo, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

            if (dtFrom == null || dtTo == null)
            {
                MessageBox.Show("Please fill Date from & to", "Error");
                return;
            }
            if (dtFrom > dtTo)
            {
                MessageBox.Show("Date from can not be greater than date to.", "Error");
                return;
            }

            //CommonLibrary.Instance.createLogFile(path, ipMachine);
            CommonLibrary.Instance.createLogFile(path, ipMachine);

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Reading data from machine " + ipMachine;
            CommonLibrary.Instance.writeLogApp(messages);

            Cursor = Cursors.WaitCursor;
            lvLogs.Items.Clear();

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                               out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    //msg = string.Format("Reading {0} from total records {1}", j.ToString() ,iValue.ToString());
                    //lblStatus.Text = msg;

                    attendanceDate = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString()));
                    DateTime attDate = DateTime.ParseExact(attendanceDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                    if (attDate >= dtFrom && attDate <= dtTo)
                    {
                        iGLCount++;

                        lvLogs.Items.Add(iGLCount.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(sdwEnrollNumber.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2") + " " + idwHour.ToString("D2") + ":" + idwMinute.ToString("D2") + ":" + idwSecond.ToString("D2"));
                        attendanceDate = idwYear.ToString() + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2") + " " + idwHour.ToString("D2") + ":" + idwMinute.ToString("D2") + ":" + idwSecond.ToString("D2");
                        iIndex++;

                        enrollNo = Convert.ToInt32(sdwEnrollNumber);
                        status = idwInOutMode;

                        httpString += ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipMachine);
                        //data += ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipMachine);
                        data += ((iGLCount == 1) ? "" : Environment.NewLine ) + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipMachine);

                        //using (var tw = new StreamWriter(path, true))
                        //{
                        //    tw.WriteLine(data);
                        //    tw.Close();
                        //}
                    }

                    j++;
                }
                CommonLibrary.Instance.writeLog(path, data);

                dataAttendance = httpString;
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");

                    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString();
                    CommonLibrary.Instance.writeLogApp(messages);
                }
                else
                {
                    MessageBox.Show("No data from terminal returns!", "Error");

                    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "No data from terminal returns!";
                    CommonLibrary.Instance.writeLogApp(messages);
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
            dataAttendance = httpString;
            MessageBox.Show("Pull Data Complete", "Information");
            btnPush.Enabled = true;
        }
        #endregion

        #region Get AttLogs
        private void getAttLogs(string ipMachine, int seqNo)
        {
            string sdwEnrollNumber = "";
            string attendanceDate = "";
            string ipAddress = ipMachine;
            string data = "";
            
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;
            int idwErrorCode = 0;
            int iGLCount = lvLogs.Items.Count;
            int iIndex = lvLogs.Items.Count;
            int iGLCountData = 0;
            int enrollNo = 0;
            int status = 0;

            //string path = apppath + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            string path = apppath + @"\log\ManualPullData_" + ipMachine + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            string today = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            string _dtFrom = dtPickerFrom.Value.ToString("yyyy-MM-dd");
            string _dtTo = dtPickerTo.Value.ToString("yyyy-MM-dd");

            DateTime dtFrom = DateTime.ParseExact(_dtFrom, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(_dtTo, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

            if (dtFrom == null || dtTo == null)
            {
                MessageBox.Show("Please fill Date from & to", "Error");
                return;
            }
            if (dtFrom > dtTo)
            {
                MessageBox.Show("Date from can not be greater than date to.", "Error");
                return;
            }

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Reading data from machine " + ipMachine;
            CommonLibrary.Instance.writeLogApp(messages);

            //CommonLibrary.Instance.createLogFile(path, ipMachine);
            CommonLibrary.Instance.createLogFile(path, ipMachine);

            Cursor = Cursors.WaitCursor;
            //lvLogs.Items.Clear();

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                               out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {

                    attendanceDate = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString()));
                    DateTime attDate = DateTime.ParseExact(attendanceDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                    //if (today == date_att) {
                    if (attDate >= dtFrom && attDate <= dtTo)
                    {

                        //test untuk dummy dengan ip yang sama
                        //sdwEnrollNumber = (seqNo > 1) ? ( "100"+sdwEnrollNumber.ToString() ) : sdwEnrollNumber ;
                        //ipAddress = (seqNo > 1) ? (ipMachine + seqNo.ToString()) : ipMachine;
                        //test untuk dummy dengan ip yang sama

                        iGLCount++;
                        iGLCountData++;

                        lvLogs.Items.Add(iGLCount.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(sdwEnrollNumber.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                        lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2") + " " + idwHour.ToString("D2") + ":" + idwMinute.ToString("D2") + ":" + idwSecond.ToString("D2"));
                        attendanceDate = idwYear.ToString() + "-" + idwMonth.ToString("D2") + "-" + idwDay.ToString("D2") + " " + idwHour.ToString("D2") + ":" + idwMinute.ToString("D2") + ":" + idwSecond.ToString("D2");
                        iIndex++;

                        enrollNo = Convert.ToInt32(sdwEnrollNumber);
                        status = idwInOutMode;

                        _httpString += ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipAddress);
                        //_data = ((iGLCount == 1) ? "" : ";") + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipAddress);
                        data += ((iGLCountData == 1) ? "" : Environment.NewLine) + string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, ipMachine);

                        //using (var tw = new StreamWriter(path, true))
                        //{
                        //    tw.WriteLine(_data);
                        //    tw.Close();
                        //}
                    }
                }

                CommonLibrary.Instance.writeLog(path, data);

                dataAttendance = _httpString;
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");

                    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString();
                    CommonLibrary.Instance.writeLogApp(messages);
                }
                else
                {
                    MessageBox.Show("No data from terminal returns!", "Error");


                    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + "No data from terminal returns!";
                    CommonLibrary.Instance.writeLogApp(messages);
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
            axCZKEM1.Disconnect();
        }

        #endregion

        private void fillComboBox()
        {
            List<ComboboxItem> data = listIpMachine;
            data.Insert(0, new ComboboxItem { Text = "All", Value = "All" });
            cmbIpMachine.DataSource = listIpMachine;
            cmbIpMachine.SelectedIndex = 0;
        }

       
    }
} 